// cell should be in charge of making cell things, like element; it;s subordinate to cell; want to be in charge of cell calss only
class Cell {
    // particular
    constructor (rowIndex, colIndex, cellClasses) {
        this.rowIndex = rowIndex;
        this.colIndex = colIndex;
        this.cellClasses = cellClasses;
        this.element = this.createElement();
    }

    // generals 
    createElement() {
        const element = document.createElement("div");
        element.classList.add("cell", ...this.cellClasses);
        element.dataset.rowIndex = this.rowIndex;
        element.dataset.colIndex = this.colIndex;
        return element;
    }
}