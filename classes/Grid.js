class Grid {
    // particulars through consructor; function that runs when do new; anything else that happens at that moment with grid should start here
    constructor (options) {
        // this can vary, so have here
        this.numberOfRows = options.numberOfRows;
        this.numberOfColumns = options.numberOfColumns;
        this.targetElement = options.targetElement || document.body;
        this.cellClasses = options.cellClasses || [];
        this.gridElement = this.createGridElement();
        // 2d model array to represent dom; outer hadnum of arrays with internal each as row or colkumn; outer array could be rows and individual were columns/cell; easier in dom to do this cause rows are div which are display block b/c trying to mimic the way english text is represented 
        // nested array is created by loops (one for inner (colums) and one for outer (rows)); instanciate on creation
        this.rows = [];
        this.createRows();
    }

    // general for all prototypes; methods
    createGridElement() {
        const element = document.createElement("div");
        element.classList.add("grid");
        this.targetElement.appendChild(element);
        return element;
    }
    createRowElement(rowIndex) {
        const element = document.createElement("div");
        element.classList.add("row");
        element.dataset.rowIndex = rowIndex;
        // grid attaches to target lemenet 
        this.gridElement.appendChild(element);
        return element;
    }
    // need to popualte row []
    createRows() {
        for (let rowIndex = 0; rowIndex < this.numberOfRows; rowIndex++) {
            // now that it is an array, give ti the index (starts at 0) and makes reach row empty array; if num num is four, it will do [],[],[],[]
            this.rows[rowIndex] = [];
            // want row div to go on target or  make stuff to connect easier to css
            // dom rep of row; has class row and dataclass index
            const rowElement = this.createRowElement(rowIndex);
            // what row working on and what elmenet ot attach cell to
            this.createCells(rowIndex, rowElement)
        }
    }
    createCells(rowIndex, rowElement) {
        for (let colIndex = 0; colIndex < this.numberOfColumns; colIndex++) {
            // needs to know row and ccolumn index; cell ishould know its own place
            // is incharge of 
            const cell = new Cell(rowIndex, colIndex, this.cellClasses);
            this.rows[rowIndex][colIndex] = cell;
            // cell needs element prop to point to dom element/div that is cell; each instance has element; add cell to row; cell doesnt know about other cells, but grid is large and seems all bits 
            rowElement.appendChild(cell.element);
        }
    }
}