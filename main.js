// want to make stuff bound up in grids and cell classes to add inheritance; could add in tictactoe and maze and sokoabn

// these properties do something 
const genericGrid = new Grid({
    numberOfRows: 7,
    numberOfColumns: 6,
    targetElement: document.querySelector("main"),
    // could make diff colors by doing in passing what classes you want to use
    cellClasses: ["red-border"],
});

const genericGrid2 = new Grid({
    numberOfRows: 2,
    numberOfColumns: 2,
    targetElement: document.querySelector("main"),
    cellClasses: ["black-border"],
});

const genericGrid3 = new Grid({
    numberOfRows: 5,
    numberOfColumns: 30,
    targetElement: document.querySelector("main"),
    cellClasses: ["blue-border"],
});

// usually have @ least 1 event to get given cell, like click; want eventlistener and add that behavoir to existing classes; can provide to each individual cell with cellclass or (better) have grid element have eventlistener wich is one and do event.target (not curT) to trigger each child element since propigates as each when goes down and looks for any click in grid and if clicked on cell, it does that, so give levent listenert to grid (find cell by indexes and get access to it; row.dataset.row/colindex and look to find actual cell index that coresponds to that thing; find cell by index or element)
// checkers cell extends cell